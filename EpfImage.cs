﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Drawing;

namespace BaramRenderer
{
    class EpfImage
    {
        public class EpfImageItem
        {
            public short top, left, bottom, right;
            public long pixel, mask;

            public void Draw(Graphics g, int x, int y, int vc, Color[] c, MemoryMappedViewAccessor mmva, int alpha)
            {
                var xs = right - left;
                var ys = bottom - top;

                if (xs > 0 && ys > 0)
                {
                    var pixel = new byte[xs * ys];
                    mmva.ReadArray<byte>(this.pixel, pixel, 0, pixel.Length);

                    using (var _bitmap = new Bitmap(xs, ys, g))
                    {
                        var bitmap = new LockBitmap(_bitmap);
                        bitmap.LockBits();

                        long pos = mask;
                        for (var _y = 0; _y < ys; ++_y)
                        {
                            var _x = 0;

                            for (byte b = mmva.ReadByte(pos++); b != 0; b = mmva.ReadByte(pos++))
                            {
                                var n = b & 127;

                                if ((b & 128) != 0)
                                    for (var xx = 0; xx < n && _x + xx < xs; ++xx)
                                    {
                                        byte i = pixel[_y * xs + _x + xx];

                                        if (i >= 48)
                                            i = (byte)((int)i + vc * 8);

                                        bitmap.SetPixel(_x + xx, _y, Color.FromArgb(alpha, c[i]));
                                    }

                                _x += n;
                            }
                        }

                        bitmap.UnlockBits();
                        g.DrawImage(_bitmap, x + left, y + top);
                    }
                }
            }
        }

        private List<EpfImageItem> _items = new List<EpfImageItem>();
        private MemoryMappedViewAccessor _mmva;

        public EpfImage(DatItem dat)
        {
            using (var reader = new MyBinaryReader(dat.CreateViewStream()))
            {
                int count = reader.ReadInt16();
                reader.BaseStream.Seek(6, SeekOrigin.Current);

                long size = reader.ReadInt32();
                _mmva = dat.CreateViewAccessor(reader.BaseStream.Position, size);
                reader.BaseStream.Seek(size, SeekOrigin.Current);

                for (int i = 0; i < count; ++i)
                {
                    EpfImageItem item = new EpfImageItem();
                    item.top = reader.ReadInt16();
                    item.left = reader.ReadInt16();
                    item.bottom = reader.ReadInt16();
                    item.right = reader.ReadInt16();
                    item.pixel = reader.ReadInt32();
                    item.mask = reader.ReadInt32();
                    _items.Add(item);
                }
            }
        }

        public void Draw(Graphics g, int x, int y, int index, int vc, Color[] c, int alpha = 255)
        {
            _items[index].Draw(g, x, y, vc, c, _mmva, alpha);
        }

        public int Count
        {
            get
            {
                return _items.Count;
            }
        }
    }
}
