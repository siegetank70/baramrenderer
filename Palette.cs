﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;

namespace BaramRenderer
{
    class Palette
    {
        private List<Color[]> _items = new List<Color[]>();

        public Color[] this[int i]
        {
            get
            {
                return _items[i];
            }
        }

        public Palette(DatItem dat)
        {
            using (var reader = new MyBinaryReader(dat.CreateViewStream()))
            {
                int count = reader.ReadInt32();

                if (count == 0x61504C44)
                {
                    Append(reader);
                    return;
                }

                for (int i = 0; i < count; ++i)
                {
                    if (reader.ReadInt32() != 0x61504C44)
                        break;
                    Append(reader);
                }
            }
        }

        private void Append(MyBinaryReader reader)
        {
            reader.BaseStream.Seek(20, SeekOrigin.Current);
            int v = reader.ReadInt32();
            reader.BaseStream.Seek(4, SeekOrigin.Current);

            if (v != 0)
                reader.BaseStream.Seek(v * 2, SeekOrigin.Current);

            var c = new Color[256];
            _items.Add(c);

            for (int j = 0; j < 256; ++j)
            {
                byte r = reader.ReadByte();
                byte g = reader.ReadByte();
                byte b = reader.ReadByte();
                byte a = reader.ReadByte(); // unused
                c[j] = Color.FromArgb(r, g, b);
            }
        }
    }
}
