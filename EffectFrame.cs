using System;

namespace BaramRenderer
{
	internal class EffectFrame
	{
		public int _u1;

		public int _u2;

		public int _u3;

		public int _u4;

		public EffectFrame(MyBinaryReader reader)
		{
			this._u1 = reader.DecodeInt32();
			this._u2 = reader.DecodeInt32();
			this._u3 = reader.DecodeInt32();
			this._u4 = reader.DecodeInt32();
		}
	}
}
