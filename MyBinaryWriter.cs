﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaramRenderer
{
    class MyBinaryWriter : System.IO.BinaryWriter
    {
        public MyBinaryWriter(System.IO.Stream stream)
            : base(stream)
        {
        }

        public void WriteString(string value, int length)
        {
            byte[] a = new byte[length], b = Encoding.Default.GetBytes(value);
            Array.Copy(b, a, (b.Length > length) ? length : b.Length);
            a[length - 1] = 0;
            Write(a);
        }

        public void Write1String(string value)
        {
            byte[] a = Encoding.Default.GetBytes(value);

            if (a.Length > 255)
            {
                Write((byte)255);
                Write(a, 0, 255);
            }
            else
            {
                Write((byte)a.Length);
                Write(a);
            }
        }

        public void Write4String(string value)
        {
            byte[] a = Encoding.Default.GetBytes(value);
            Write(a.Length);
            Write(a);
        }
    }
}
