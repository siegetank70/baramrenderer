﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BaramRenderer
{
    class CharacterRenderer
    {
        class PartImage
        {
            public int _u1 = 0, _u2 = 0, _u3 = 0;

            public PartImage(MyBinaryReader reader)
            {
                _u1 = reader.DecodeInt32(); // num
                _u2 = reader.DecodeInt32(); // palette
                _u3 = reader.DecodeInt32(); // base
            }
        }

        private EpfImage _epfImage_HEAD = null;
        private EpfImage _epfImage_HEADSP = null;
        private EpfImage _epfImage_BODY = null;
        private EpfImage _epfImage_SWORD = null;
        private EpfImage _epfImage_SPEAR = null;
        private EpfImage _epfImage_FAN = null;
        private EpfImage _epfImage_SHIELD = null;
        private Palette _palette_HEAD = null;
        private Palette _palette_HEADSP = null;
        private Palette _palette_BODY = null;
        private Palette _palette_SWORD = null;
        private Palette _palette_SPEAR = null;
        private Palette _palette_FAN = null;
        private Palette _palette_SHIELD = null;
        private List<PartImage> _items_HEAD = new List<PartImage>();
        private List<PartImage> _items_HEADSP = new List<PartImage>();
        private List<PartImage> _items_BODY = new List<PartImage>();
        private List<PartImage> _items_SWORD = new List<PartImage>();
        private List<PartImage> _items_SPEAR = new List<PartImage>();
        private List<PartImage> _items_FAN = new List<PartImage>();
        private List<PartImage> _items_SHIELD = new List<PartImage>();
        private List<string> _drworder = new List<string>();

        public CharacterRenderer(DatFile datFile)
        {
            // TODO: DRWORDER.TBL

            _epfImage_HEAD = new EpfImage(datFile.Lookup("head.epf"));
            _epfImage_HEADSP = new EpfImage(datFile.Lookup("headsp.epf"));
            _epfImage_BODY = new EpfImage(datFile.Lookup("body.epf"));
            _epfImage_SWORD = new EpfImage(datFile.Lookup("sword.epf"));
            _epfImage_SPEAR = new EpfImage(datFile.Lookup("spear.epf"));
            _epfImage_FAN = new EpfImage(datFile.Lookup("fan.epf"));
            _epfImage_SHIELD = new EpfImage(datFile.Lookup("shield.epf"));

            _palette_HEAD = new Palette(datFile.Lookup("head.pal"));
            _palette_HEADSP = new Palette(datFile.Lookup("headsp.pal"));
            _palette_BODY = new Palette(datFile.Lookup("body.pal"));
            _palette_SWORD = new Palette(datFile.Lookup("sword.pal"));
            _palette_SPEAR = new Palette(datFile.Lookup("spear.pal"));
            _palette_FAN = new Palette(datFile.Lookup("fan.pal"));
            _palette_SHIELD = new Palette(datFile.Lookup("shield.pal"));

            using (var reader = new MyBinaryReader(datFile.Lookup("head.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items_HEAD.Add(new PartImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("headsp.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items_HEADSP.Add(new PartImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("body.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items_BODY.Add(new PartImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("sword.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items_SWORD.Add(new PartImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("spear.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items_SPEAR.Add(new PartImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("fan.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items_FAN.Add(new PartImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("shield.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items_SHIELD.Add(new PartImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("drworder.tbl").CreateViewStream()))
            {
                for (;;)
                {
                    var a = reader.ReadChar();
                    if (a != 'w' && a != 'b' && a != 'h' && a != 's' && a != '0')
                        break;
                    var b = reader.ReadChar();
                    if (b != 'w' && b != 'b' && b != 'h' && b != 's' && b != '0')
                        break;
                    var c = reader.ReadChar();
                    if (c != 'w' && c != 'b' && c != 'h' && c != 's' && c != '0')
                        break;
                    var d = reader.ReadChar();
                    if (d != 'w' && d != 'b' && d != 'h' && d != 's' && d != '0')
                        break;
                    var e = reader.ReadChar();
                    if (e != ';')
                        break;
                    var s = "" + a + b + c + d;
                    _drworder.Add(s);
                }
            }
        }

        public void Draw(Graphics g, int x, int y, int head, int headc, int body, int bodyc, int weapon, int weaponc, int shield, int shieldc, int frame)
        {
            g.Clear(Color.Gray);

            // 1: 00000 ~ 09999 (SWORD)
            // 2: 10000 ~ 19999 (SPEAR)
            // 3: 20000 ~ 29999 ()
            // 4: 30000 ~ 39999 (FAN)

            if (frame >= 0 && frame < _drworder.Count)
            {
                var frame_104 = frame % 104;

                foreach (var o in _drworder[frame])
                {
                    switch (o)
                    {
                        case 'b':
                            // URDL 순
                            //  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11: 비무장 MOVE
                            // 12,13,14,15,16,17,18,19,20,21,22,23: 이동(무장-SWORD)
                            // 24,25,26,27,28,29,30,31: 공격(SWORD)
                            // 32,33,34,35,36,37,38,39,40,41,42,43: 이동(무장-SPEAR)
                            // 44,45,46,47,48,49,50,51: 공격(SPEAR)
                            // 52,53,54,55,56,57,58,59,60,61,62,63: 이동(무장-BOW)
                            // 64,65,66,67,68,69,70,71: 공격(BOW)
                            // 72,73,74,75: 주문
                            // 76,77,78,79: 앉기
                            // 80,81,82,83,84,85,86,87,88,89,90,91: 이동 (말)
                            // 92: 처묵
                            // 93: 손들기
                            // 94: ?
                            // 95: 으쓱
                            // 96,97,97,99: 꾸벅
                            // 100,101,102,103: ?

                            if (body >= 0 && body < _items_BODY.Count)
                            {
                                var i = _items_BODY[body]; // 104개씩
                                _epfImage_BODY.Draw(g, x, y, i._u3 + frame, bodyc, _palette_BODY[i._u2]);
                            }
                            break;

                        case 'h':
                            if (head >= 0 && head < _items_HEAD.Count)
                            {
                                var i = _items_HEAD[head]; // 100개씩
                                if (frame_104 >= 0 && frame_104 <= 100)
                                    _epfImage_HEAD.Draw(g, x, y, i._u3 + frame_104, headc, _palette_HEAD[i._u2]);
                            }
                            break;

                        case 's':
                            if (shield >= 0 && shield < _items_SHIELD.Count)
                            {
                                var i = _items_SHIELD[shield]; // 20개씩
                                if (frame_104 >= 0 && frame_104 <= 11)
                                    _epfImage_SHIELD.Draw(g, x, y, i._u3 + frame_104, shieldc, _palette_SHIELD[i._u2]);
                                else if (frame_104 >= 12 && frame_104 <= 31)
                                    _epfImage_SHIELD.Draw(g, x, y, i._u3 + frame_104 - 12, shieldc, _palette_SHIELD[i._u2]);
                            }
                            break;

                        case 'w':
                            if (weapon >= 0 && weapon < 10000)
                            {
                                    if (weapon >= 0 && weapon < _items_SWORD.Count)
                                    {
                                        var i = _items_SWORD[weapon]; // 20개씩
                                        if (frame_104 >= 12 && frame_104 <= 31)
                                            _epfImage_SWORD.Draw(g, x, y, i._u3 + frame_104 - 12, weaponc, _palette_SWORD[i._u2]);
                                    }
                            }
                            else if (weapon >= 10000 && weapon < 20000)
                            {
                                weapon -= 10000;
                                if (weapon >= 0 && weapon < _items_SPEAR.Count)
                                {
                                    var i = _items_SPEAR[weapon]; // 20개씩
                                    if (frame_104 >= 32 && frame_104 <= 51)
                                        _epfImage_SPEAR.Draw(g, x, y, i._u3 + frame_104 - 32, weaponc, _palette_SPEAR[i._u2]);
                                }
                            }
                            else if (weapon >= 30000 && weapon < 40000)
                            {
                                weapon -= 30000;
                                if (weapon >= 0 && weapon < _items_FAN.Count)
                                {
                                    var i = _items_FAN[weapon]; // 20개씩
                                    if (frame_104 >= 12 && frame_104 <= 31)
                                        _epfImage_FAN.Draw(g, x, y, i._u3 + frame_104 - 12, weaponc, _palette_FAN[i._u2]);
                                }
                            }

                            break;
                    }
                }
            }
        }
    }
}
