﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.MemoryMappedFiles;

namespace BaramRenderer
{
    public class DatItem
    {
        public int offset;
        public int length;
        public string name;
        public MemoryMappedFile mmf;

        public MemoryMappedViewStream CreateViewStream()
        {
            return mmf.CreateViewStream(offset, length, MemoryMappedFileAccess.Read);
        }

        public MemoryMappedViewAccessor CreateViewAccessor(long offset, long size)
        {
            return mmf.CreateViewAccessor(this.offset + offset, size, MemoryMappedFileAccess.Read);
        }
    };

    class DatFile
    {
        private Dictionary<string, DatItem> _db = new Dictionary<string, DatItem>();

        public DatItem Lookup(string name)
        {
            DatItem item = null;

            if (!_db.TryGetValue(name.ToUpper(), out item))
                throw new Exception("파일이 없습니다: " + name);

            return item;
        }

        public void Load(string path)
        {
            if (!File.Exists(path))
                throw new Exception("파일이 없습니다: " + path);

            var fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            var mmf = MemoryMappedFile.CreateFromFile(fs, null, 0, MemoryMappedFileAccess.Read, null, HandleInheritability.None, false);

            using (var reader = new MyBinaryReader(mmf.CreateViewStream(0, 0, MemoryMappedFileAccess.Read)))
            {
                DatItem _item = null;

                int count = reader.ReadInt32();

                for (int i = 0; i < count; ++i)
                {
                    var item = new DatItem();

                    item.offset = reader.ReadInt32();
                    item.name = reader.ReadString(13);
                    item.mmf = mmf;

                    if (_item != null)
                    {
                        _item.length = item.offset - _item.offset;
                        _db.Add(_item.name.ToUpper(), _item);
                    }

                    _item = item;
                }
            }
        }
    }
}
