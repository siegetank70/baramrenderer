using System;

namespace BaramRenderer
{
	internal class MonsterActionFrame
	{
		public int _u1;

		public int _u2;

		public int _u3;

		public int _u4;

		public int _u5;

		public int _u6;

		public MonsterActionFrame(MyBinaryReader reader)
		{
			this._u1 = (int)reader.ReadUInt16();
			this._u2 = (int)reader.ReadUInt16();
			this._u3 = (int)reader.ReadUInt16();
			this._u4 = (int)reader.ReadByte();
			this._u5 = (int)reader.ReadByte();
			this._u6 = (int)reader.ReadByte();
		}
	}
}
