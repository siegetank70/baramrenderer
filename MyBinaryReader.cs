﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaramRenderer
{
    class MyBinaryReader : System.IO.BinaryReader
    {
        public MyBinaryReader(System.IO.Stream stream)
            : base(stream)
        {
        }

        public string ReadString(int length)
        {
            if (length <= 0)
                return "";

            byte[] a = new byte[length];
            Read(a, 0, length);

            if (a[0] == 0)
                return "";

            int count = Array.IndexOf<byte>(a, 0);

            if (count < 0)
                count = length;

            return Encoding.Default.GetString(a, 0, count);
        }

        public string Read1String()
        {
            return ReadString(ReadByte());
        }

        public string Read4String()
        {
            return ReadString(ReadInt32());
        }

        public int DecodeInt32()
        {
            var key = new byte[] {
                0x4B, 0x19, 0x1F, 0x1D, 0x1A, 0x09, 0x0C, 0x0C,
		        0x53, 0x49, 0x13, 0x11, 0x1D, 0x17, 0x06, 0x1D,
		        0x09, 0x06, 0x08, 0x1B, 0x1C, 0x01, 0x1E, 0x1D,
		        0x03, 0x05, 0x09
            };

            for (var i = 0; i < 26; ++i)
                key[i + 1] ^= key[i];

            var pos = BaseStream.Position / 2;
            var buf = ReadBytes(8);

            var idx = (0xFEDCBA98 - pos) % 27;
            for (var i = 0; i < 8; ++i)
            {
                buf[i] ^= key[idx];
                idx = (idx + 26) % 27;
            }

            buf = buf.Reverse().ToArray();

            long a = BitConverter.ToInt32(buf, 0);
            long b = BitConverter.ToInt32(buf, 4);

            if (pos != ((a & 0xAAAAAAAA) | (b & 0x55555555)))
                throw new Exception("Invalid Decode Position");

            return (int)((a & 0x55555555) | (b & 0xAAAAAAAA));
        }
    }
}
