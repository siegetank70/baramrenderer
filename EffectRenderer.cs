﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BaramRenderer
{
    class EffectFrame
    {
        public int _u1 = 0, _u2 = 0, _u3 = 0, _u4 = 0;

        public EffectFrame(MyBinaryReader reader)
        {
            _u1 = reader.DecodeInt32(); // image
            _u2 = reader.DecodeInt32(); // delay
            _u3 = reader.DecodeInt32(); // 0
            _u4 = reader.DecodeInt32(); // -1
        }
    };

    class EffectImage
    {
        public int _u1 = 0, _u2 = 0, _u3 = 0, _u4 = 0, _u5 = 0, _u6 = 0, _u7 = 0;
        public List<EffectFrame> _frames1 = new List<EffectFrame>();
        public List<EffectFrame> _frames2 = new List<EffectFrame>();

        public EffectImage(MyBinaryReader reader)
        {
            _u1 = reader.DecodeInt32(); // num
            _u2 = reader.DecodeInt32();
            _u3 = reader.DecodeInt32(); // -1
            _u4 = reader.DecodeInt32(); // -1
            _u5 = reader.DecodeInt32();
            _u6 = reader.DecodeInt32(); // -1
            _u7 = reader.DecodeInt32(); // -1
            for (int i = 0; i < _u2; ++i)
                _frames1.Add(new EffectFrame(reader));
            for (int i = 0; i < _u5; ++i)
                _frames2.Add(new EffectFrame(reader));
            if (_u3 != -1 || _u4 != -1 || _u6 != -1 || _u7 != -1)
            {
                var u = _u3 ^ _u4 ^ _u6 ^ _u7;
            }
        }
    }

    class EffectRenderer
    {
        private EpfImage _epfImage = null;
        private Palette _palette = null;
        private List<EffectImage> _items = new List<EffectImage>();
        private List<int> _palette_ref = new List<int>();

        public EffectRenderer(DatFile datFile)
        {
            _epfImage = new EpfImage(datFile.Lookup("effect.epf"));
            _palette = new Palette(datFile.Lookup("effect.pal"));

            using (var reader = new MyBinaryReader(datFile.Lookup("effect.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items.Add(new EffectImage(reader));
            }

            using (var reader = new MyBinaryReader(datFile.Lookup("effect.frm").CreateViewStream()))
            {
                var count = reader.ReadInt32();
                for (var i = 0; i < count; ++i)
                    _palette_ref.Add(reader.ReadInt32());
            }
        }

        public void Draw(Graphics g, int x, int y, int image, int frame)
        {
            g.Clear(Color.Gray);

            if (image >= 0 && image < _items.Count)
            {
                var i = _items[image];

                if (frame == -1)
                {
                    // TODO: 얘네 동시에 진행해야 됨
                    foreach (var f in i._frames2)
                    {
                        g.Clear(Color.Gray);

                        if (f._u1 != -1)
                            _epfImage.Draw(g, x, y, f._u1, 0, _palette[_palette_ref[f._u1]]);

                        System.Threading.Thread.Sleep(f._u2);
                    }
                    foreach (var f in i._frames1)
                    {
                        g.Clear(Color.Gray);

                        if (f._u1 != -1)
                            _epfImage.Draw(g, x, y, f._u1, 0, _palette[_palette_ref[f._u1]]);

                        System.Threading.Thread.Sleep(f._u2);
                    }
                    return;
                }

                if (frame >= 0 && frame < i._frames2.Count)
                {
                    var f = i._frames2[frame];
                    if (f._u1 != -1)
                        _epfImage.Draw(g, x, y, f._u1, 0, _palette[_palette_ref[f._u1]]);
                } 

                frame -= i._frames1.Count;

                if (frame >= 0 && frame < i._frames1.Count)
                {
                    var f = i._frames1[frame];
                    if (f._u1 != -1)
                        _epfImage.Draw(g, x, y, f._u1, 0, _palette[_palette_ref[f._u1]]);
                }
            }
        }

        public int Count
        {
            get
            {
                return _items.Count;
            }
        }
    }
}
