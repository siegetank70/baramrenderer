﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaramRenderer
{
    public partial class Form1 : Form
    {
        private DatFile _datFile = null;
        private MonsterRenderer _monsterRenderer = null;
        private ItemRenderer _itemRenderer = null;
        private EffectRenderer _effectRenderer = null;
        private CharacterRenderer _characterRenderer = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string root = ""; //  @"C:\Users\mina\Desktop\Baram550\";

                folderBrowserDialog1.Description = "바람의나라 경로를 골라라(mon.dat가 있는곳)";

                if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    root = folderBrowserDialog1.SelectedPath + "\\";
                }

                _datFile = new DatFile();
                _datFile.Load(root + "misc.dat");
                _datFile.Load(root + "char.dat");
                _datFile.Load(root + "tile.dat");
                _datFile.Load(root + "mon.dat");
                _datFile.Load(root + "efx.dat");
                // snd.dat
                // mus000~mus999.dat
                _datFile.Load(root + "baram.dat");
                _datFile.Load(root + "bint.dat");

                _monsterRenderer = new MonsterRenderer(_datFile);
                _itemRenderer = new ItemRenderer(_datFile);
                _effectRenderer = new EffectRenderer(_datFile);
                _characterRenderer = new CharacterRenderer(_datFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Application.Exit();
            }
        }

        private void DrawMonster(int image, int color, int action, int frame)
        {
            using (var g = pictureBox1.CreateGraphics())
            {
                _monsterRenderer.Draw(g, 320, 240, image, color, action, frame);
            }
        }

        private void DrawItem(int image, int color)
        {
            using (var g = pictureBox1.CreateGraphics())
            {
                _itemRenderer.Draw(g, 320, 240, image, color);
            }
        }

        private void DrawEffect(int image, int frame)
        {
            using (var g = pictureBox1.CreateGraphics())
            {
                _effectRenderer.Draw(g, 320, 240, image, frame);
            }
        }

        private void DrawCharacter(int head, int headc, int body, int bodyc, int weapon, int weaponc, int shield, int shieldc, int frame)
        {
            using (var g = pictureBox1.CreateGraphics())
            {
                _characterRenderer.Draw(g, 320, 240, head, headc, body, bodyc, weapon, weaponc, shield, shieldc, frame);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DrawMonster((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, -1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DrawItem((int)numericUpDown5.Value, (int)numericUpDown6.Value);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DrawEffect((int)numericUpDown7.Value, -1);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            DrawMonster((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            DrawMonster((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            DrawMonster((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value);
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            DrawMonster((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value);
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            DrawItem((int)numericUpDown5.Value, (int)numericUpDown6.Value);
        }

        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            DrawItem((int)numericUpDown5.Value, (int)numericUpDown6.Value);
        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            DrawEffect((int)numericUpDown7.Value, (int)numericUpDown8.Value);
        }

        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            DrawEffect((int)numericUpDown7.Value, (int)numericUpDown8.Value);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown9_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown10_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown11_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown12_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown13_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown14_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown15_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown16_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }

        private void numericUpDown17_ValueChanged(object sender, EventArgs e)
        {
            DrawCharacter((int)numericUpDown9.Value, (int)numericUpDown10.Value, (int)numericUpDown11.Value, (int)numericUpDown12.Value, (int)numericUpDown13.Value, (int)numericUpDown14.Value, (int)numericUpDown15.Value, (int)numericUpDown16.Value, (int)numericUpDown17.Value);
        }
    }
}