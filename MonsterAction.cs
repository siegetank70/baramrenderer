using System;
using System.Collections.Generic;

namespace BaramRenderer
{
	internal class MonsterAction
	{
		public List<MonsterActionFrame> _frames = new List<MonsterActionFrame>();

		public MonsterAction(MyBinaryReader reader)
		{
			int num = (int)reader.ReadUInt16();
			for (int i = 0; i < num; i++)
			{
				this._frames.Add(new MonsterActionFrame(reader));
			}
		}
	}
}
