using System;
using System.Collections.Generic;

namespace BaramRenderer
{
	internal class EffectImage
	{
		public int _u1;

		public int _u2;

		public int _u3;

		public int _u4;

		public int _u5;

		public int _u6;

		public int _u7;

		public List<EffectFrame> _frames1 = new List<EffectFrame>();

		public List<EffectFrame> _frames2 = new List<EffectFrame>();

		public EffectImage(MyBinaryReader reader)
		{
			this._u1 = reader.DecodeInt32();
			this._u2 = reader.DecodeInt32();
			this._u3 = reader.DecodeInt32();
			this._u4 = reader.DecodeInt32();
			this._u5 = reader.DecodeInt32();
			this._u6 = reader.DecodeInt32();
			this._u7 = reader.DecodeInt32();
			for (int i = 0; i < this._u2; i++)
			{
				this._frames1.Add(new EffectFrame(reader));
			}
			for (int j = 0; j < this._u5; j++)
			{
				this._frames2.Add(new EffectFrame(reader));
			}
			if (this._u3 != -1 || this._u4 != -1 || this._u6 != -1 || this._u7 != -1)
			{
				int arg_DE_0 = this._u3;
				int arg_E5_0 = this._u4;
				int arg_EC_0 = this._u6;
				int arg_F3_0 = this._u7;
			}
		}
	}
}
