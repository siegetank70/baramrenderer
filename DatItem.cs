using System;
using System.IO.MemoryMappedFiles;

namespace BaramRenderer
{
	public class DatItem
	{
		public int offset;

		public int length;

		public string name;

		public MemoryMappedFile mmf;

		public MemoryMappedViewStream CreateViewStream()
		{
			return this.mmf.CreateViewStream((long)this.offset, (long)this.length, MemoryMappedFileAccess.Read);
		}

		public MemoryMappedViewAccessor CreateViewAccessor(long offset, long size)
		{
			return this.mmf.CreateViewAccessor((long)this.offset + offset, size, MemoryMappedFileAccess.Read);
		}
	}
}
