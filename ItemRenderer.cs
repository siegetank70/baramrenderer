﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BaramRenderer
{
    class ItemImage
    {
        public int _u1 = 0, _u2 = 0, _u3 = 0, _u4 = 0, _u5 = 0;

        public ItemImage(MyBinaryReader reader)
        {
            _u1 = reader.DecodeInt32(); // tile
            _u2 = reader.DecodeInt32(); // palette
            _u3 = reader.DecodeInt32();
            _u4 = reader.DecodeInt32();
            _u5 = reader.DecodeInt32();
            if (_u5 != 0)
            {
                var u = _u3 ^ _u4 ^ _u5;
            }
        }
    }

    class ItemRenderer
    {
        private EpfImage _epfImage = null;
        private Palette _palette = null;
        private List<ItemImage> _items = new List<ItemImage>();

        public ItemRenderer(DatFile datFile)
        {
            _epfImage = new EpfImage(datFile.Lookup("item.epf"));
            _palette = new Palette(datFile.Lookup("item.pal"));

            using (var reader = new MyBinaryReader(datFile.Lookup("item.tbl").CreateViewStream()))
            {
                var count = reader.DecodeInt32();
                for (var i = 0; i < count; ++i)
                    _items.Add(new ItemImage(reader));
            }
        }

        public void Draw(Graphics g, int x, int y, int image, int color)
        {
            g.Clear(Color.Gray);

            if (image >= 0 && image < _items.Count)
            {
                var i = _items[image];
                _epfImage.Draw(g, x, y, i._u1, color, _palette[i._u2]);
            }
        }

        public int Count
        {
            get
            {
                return _items.Count;
            }
        }
    }
}
