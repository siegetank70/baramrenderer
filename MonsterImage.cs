using System;
using System.Collections.Generic;

namespace BaramRenderer
{
	internal class MonsterImage
	{
		public int _u1;

		public int _u2;

		public int _u3;

		public List<MonsterAction> _actions = new List<MonsterAction>();

		public MonsterImage(MyBinaryReader reader)
		{
			this._u1 = reader.ReadInt32();
			int num = (int)reader.ReadByte();
			this._u2 = (int)reader.ReadByte();
			this._u3 = (int)reader.ReadUInt16();
			for (int i = 0; i < num; i++)
			{
				this._actions.Add(new MonsterAction(reader));
			}
		}
	}
}
