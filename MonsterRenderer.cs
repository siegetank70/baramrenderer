﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BaramRenderer
{
    class MonsterActionFrame
    {
        public int _u1, _u2, _u3, _u4, _u5, _u6;

        public MonsterActionFrame(MyBinaryReader reader)
        {
            _u1 = reader.ReadUInt16(); // image
            _u2 = reader.ReadUInt16(); // delay
            _u3 = reader.ReadUInt16();
            _u4 = reader.ReadByte(); // alpha
            _u5 = reader.ReadByte();
            _u6 = reader.ReadByte();
        }
    };

    class MonsterAction
    {
        public List<MonsterActionFrame> _frames = new List<MonsterActionFrame>();

        public MonsterAction(MyBinaryReader reader)
        {
            // 0=숨짐
            // 1, 5, 9,13,17,21=  위로, 이동, 맞음, 공격, #2, #3
            // 2, 6,10,14,18,22=오른쪽, 이동, 맞음, 공격, #2, #3
            // 3, 7,11,15,19,23=  아래, 이동, 맞음, 공격, #2, #3
            // 4, 8,12,16,20,24=  왼쪽, 이동, 맞음, 공격, #2, #3

            int frames = reader.ReadUInt16();
            for (var i = 0; i < frames; ++i)
                _frames.Add(new MonsterActionFrame(reader));
        }
    };

    class MonsterImage
    {
        public int _u1 = 0, _u2 = 0, _u3 = 0;
        public List<MonsterAction> _actions = new List<MonsterAction>();

        public MonsterImage(MyBinaryReader reader)
        {
            _u1 = reader.ReadInt32(); // base
            int actions = reader.ReadByte(); // 9, 17, 21, 25
            _u2 = reader.ReadByte();
            _u3 = reader.ReadUInt16(); // palette
            for (var i = 0; i < actions; ++i)
                _actions.Add(new MonsterAction(reader));
        }
    }

    class MonsterRenderer
    {
        private EpfImage _epfImage = null;
        private Palette _palette = null;
        private List<MonsterImage> _items = new List<MonsterImage>();

        public MonsterRenderer(DatFile datFile)
        {
            _epfImage = new EpfImage(datFile.Lookup("monster.epf"));
            _palette = new Palette(datFile.Lookup("monster.pal"));

            using (var reader = new MyBinaryReader(datFile.Lookup("monster.dna").CreateViewStream()))
            {
                int count = reader.ReadInt32();
                for (var i = 0; i < count; ++i)
                    _items.Add(new MonsterImage(reader));
            }
        }

        public void Draw(Graphics g, int x, int y, int image, int color, int action, int frame)
        {
            g.Clear(Color.Gray);

            if (image >= 0 && image < _items.Count)
            {
                var i = _items[image];
                if (action >= 0 && action < i._actions.Count)
                {
                    var a = i._actions[action];

                    if (frame == -1)
                    {
                        foreach (var f in a._frames)
                        {
                            g.Clear(Color.Gray);

                            if (f._u1 != -1)
                                _epfImage.Draw(g, x, y, i._u1 + f._u1, 0, _palette[i._u3], 255 - f._u4);

                            var idx = a._frames.IndexOf(f);
                            if (idx == 0 || idx > 4) // stand는 sleep를 안함
                                System.Threading.Thread.Sleep(f._u2);
                        }
                        return;
                    }

                    if (frame >= 0 && frame < a._frames.Count)
                    {
                        var f = a._frames[frame];
                        _epfImage.Draw(g, x, y, i._u1 + f._u1, color, _palette[i._u3]);
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                return _items.Count;
            }
        }
    }
}
